﻿#include<iostream>

using namespace std;

const int var = 5;

class stack
{
public:

	int x[var];
	int y;
};

void push(stack &s, int x)
{
	if (s.y == var)
	{
		cout << "Stack";
		return;
	}
 s.x[s.y++] = x;
}
int pop(stack&s)
{
	return s.x[--s.y];
}

int main()
{
	stack a;
	a.y = 0;
	push(a, 1);
	push(a, 2);
	push(a, 3);
	cout << pop(a) << " ";
	cout << pop(a) << " ";
	cout << pop(a) << " ";

	return 0;
}
